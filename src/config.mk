## slock(1) version.
VERSION = 1.4

## Installation paths for binaries and man pages.
PREFIX    = /usr/local
MANPREFIX = ${PREFIX}/share/man

## Linker flags for the X11 library.
X11INC = /usr/X11R6/include
X11LIB = /usr/X11R6/lib

## Linker command-line arguments.
INCS = -I. -I/usr/include -I${X11INC}
LIBS = -L/usr/lib -lc -lcrypt -L${X11LIB} -lX11 -lXext -lXrandr

## Linker command-line arguments (OpenBSD).
#LIBS = -L/usr/lib -lc -L${X11LIB} -lX11 -lXext -lXrandr

## C++ and C command-line arguments.
CPPFLAGS = -DVERSION=\"${VERSION}\" -D_DEFAULT_SOURCE -DHAVE_SHADOW_H
CFLAGS = -std=c99 -pedantic -Wall -Os ${INCS} ${CPPFLAGS}
LDFLAGS = -s ${LIBS}
COMPATSRC = explicit_bzero.c

## C++ and C command-line arguments (NetBSD).
#CPPFLAGS = -DVERSION=\"${VERSION}\" -D_BSD_SOURCE -D_NETBSD_SOURCE

## C++ and C command-line arguments (BSD).
#CPPFLAGS = -DVERSION=\"${VERSION}\" -D_BSD_SOURCE 

## Set COMPATSRC to empty (OpenBSD).
#COMPATSRC =

## Compiler.
CC = cc
