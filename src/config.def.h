/*************
 * Apparence *
 *************/
/* Colour definitions to be used for the colour scheme. */
static const char *colorname[NUMCOLS] = {
	/* State   Foreground */
	[INIT]   = "#000000",
	[INPUT]  = "#00ffff",
	[FAILED] = "#ff0000",
	[CAPS]   = "#d7d700",
};

/* Treat a cleared input buffer like a wrong password.
 *     0: Do not change the foreground colour to "FAILED" when cleaing the input buffer.
 *     1: Do change the foreground colour to "FAILED" when cleaing the input buffer. */
static const int failonclear = 0;

/************************
 * User(s) and Group(s) *
 ************************/
/* The user and group to drop privileges to
 * when locking the screen. */
static const char *user  = "nobody";
static const char *group = "nogroup";

/*****************
 * Functionality *
 *****************/
/* The time, in seconds, before the monitor shuts down. */
static const int monitortime = 5;

/* The time, in seconds, to cancel screen locking with mouse movement. */
static const int timetocancel = 5;
