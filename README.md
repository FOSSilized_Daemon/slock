# slock - Simple X Screen Locker

```
$ slock [command] [-v]
```

# Description
**slock** is a simple X screen locker. If a command is provided to **slock**, then it will be executed after the screen has been locked.

# Arguments
* **-v**

Print the version of **slock** to standard out and then exit.

# Examples
* Run **slock** without any arguments to lock the screen.

```
$ slock
```

* Run **slock** to lock the screen, and then execute the command **srm(1)**.

```
$ slock srm
```

# Exit Status
* **0**

Successfully completed the specified task.

* **>0**

An error occured.

# See Also

* **i3lock(1)**

* **xautolock(1)**
